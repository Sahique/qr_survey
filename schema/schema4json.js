// JSON2Schema
// File for ID 'schema_id': jsoneditor_app/schema/schema4json.js
// created with JSON2Schema: https://niehausbert.gitlab.io/JSON2Schema

vDataJSON.schema4json =  {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "additionalProperties": true,
    "title": "Questionnaire Editor",
    "definitions": {
        "comment": {
            "title": "Comment:",
            "type": "string",
            "format": "textarea",
            "default": ""
        },
        "yesno": {
            "default": "yes",
            "type": "string",
            "enum": [
                "yes",
                "no"
            ]
        },
        "radio_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            "defaultProperties": [
                "title",
                "type",
                "choices",
                "Correct Answer",
                "colCount"
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "default": "",
                    "enum":["radiogroup"],
                    "format": "text",
                    "propertyOrder": 20
                },
                "choices": {
                    "type": "array",
                    "title": "Options",
                    "format": "tabs",
                    "options": {
                        "disable_collapse": false,
                        "disable_array_add": false,
                        "disable_array_delete": false,
                        "disable_array_reorder": false,
                        "disable_properties": false,
                        "collapsed": false,
                        "hidden": false
                    },
                    "items": {
                        "type": "string",
                        "id": "/properties/Options/items",
                        "title": "Options ",
                        "default": "",
                        "format": "text",
                        
                        // "options": {
                        //     "hidden": false
                        // }
                    },
                    "propertyOrder": 30
                },
                "Correct Answer": {
                    "type": "string",
                    "title": "Correct Answer",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 40
                },
                "colCount": {
                    "type": "integer",
                    "title": "no of Columns",
                    "default": "4",
                    "format": "text",
                    "propertyOrder": 50
                },
            }
        },
        "check_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            "defaultProperties": [
                "title",
                "type",
                "choices",
                "Correct Answer",
                "colCount"
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "enum":["checkbox"],
                    "title": "Option type",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                },
                "choices": {
                    "type": "array",
                    
                    "title": "Options",
                    "format": "tabs",
                    "options": {
                        "disable_collapse": false,
                        "disable_array_add": false,
                        "disable_array_delete": false,
                        "disable_array_reorder": false,
                        "disable_properties": false,
                        "collapsed": false,
                        "hidden": false
                    },
                    "items": {
                        "type": "string",
                        "id": "/properties/Options/items",
                        "title": "Option ",
                        "default": "",
                        "format": "text",
                        
                        // "options": {
                        //     "hidden": false
                        // }
                    },
                    "propertyOrder": 30
                },
                "Correct Answer": {
                    "type": "string",
                    "title": "Correct Answer",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 40
                },
                "colCount": {
                    "type": "integer",
                    
                    "title": "no of Columns",
                    "default": "4",
                    "format": "text",
                    "propertyOrder": 50
                }
            }
        },
        "dropdown_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            
            "defaultProperties": [
                "title",
                "type",
                "choices",
                "Correct Answer",
                "colCount"
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "enum":["dropdown"],
                    "title": "Option type",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                },
                "choices": {
                    "type": "array",
                    
                    "title": "Options",
                    "format": "tabs",
                    "options": {
                        "disable_collapse": false,
                        "disable_array_add": false,
                        "disable_array_delete": false,
                        "disable_array_reorder": false,
                        "disable_properties": false,
                        "collapsed": false,
                        "hidden": false
                    },
                    "items": {
                        "type": "string",
                        "id": "/properties/Options/items",
                        "title": "Option ",
                        "default": "",
                        "format": "text",
                        
                        // "options": {
                        //     "hidden": false
                        // }
                    },
                    "propertyOrder": 30
                },
                "Correct Answer": {
                    "type": "string",
                    "title": "Correct Answer",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 40
                },
                "colCount": {
                    "type": "integer",
                    
                    "title": "no of Columns",
                    "default": "0",
                    "format": "text",
                    "propertyOrder": 50
                }
            }
        },

        "text_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            
            "defaultProperties": [
                "title",
                "type",
                "placeholder",
                "isRequired"
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "enum":["text"],
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                },
                "placeholder": {
                    "type": "string",
                    "title": "Enter placeholder",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 30
                },
                "isRequired": {
                    "type": "string",
                    "title": "isRequired",
                    "default": "true",
                    "format": "text",
                    "propertyOrder": 40
                }
                
            }
        },        
        
        "boolean_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            
            "defaultProperties": [
                "type",
                "lable",
                "isRequired",
                "Correct Answer"
            ],
            "properties": {
                "lable": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "enum":["boolean"],
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                },
                "isRequired": {
                    "type": "string",
                    "title": "isRequired",
                    "default": "true",
                    "format": "text",
                    "propertyOrder": 40
                },
                "Correct Answer": {
                    "type": "string",
                    "title": "Correct Answer",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 50
                },
                
            }
        },

        "rating_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            
            "defaultProperties": [
                "title",
                "type",
                "minRateDescription",
                "maxRateDescription"
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "enum":["rating"],
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                },
                "minRateDescription": {
                    "type": "string",
                    "title": "",
                    "default": "Not Satisfied",
                    "format": "text",
                    "hidden": true,
                    "propertyOrder": 30
                },
                "maxRateDescription": {
                    "type": "string",
                    "title": "",
                    "default": "Completely satisfied",
                    "format": "text",
                    "hidden": true,
                    "propertyOrder": 40
                }
                
            }
        },

        "comment_Optionslist": {
            "type": "object",
            "id": "https://niebert.github.io/json-editor",
            "defaultProperties": [
                "title",
                "type",
            ],
            "properties": {
                "title": {
                    "type": "string",
                    "title": "Question",
                    "default": "",
                    "format": "text",
                    "propertyOrder": 10
                },
                "type": {
                    "type": "string",
                    "title": "Option type",
                    "enum":["comment"],
                    "default": "",
                    "format": "text",
                    "propertyOrder": 20
                }
                
            }
        }
    },
    "type": "object",
    "id": "https://niebert.github.io/json-editor",
    "options": {
        "disable_collapse": false,
        "disable_edit_json": false,
        "disable_properties": false,
        "collapsed": false,
        "hidden": false
    },
    "defaultProperties": [
        "Question_Bank",
        "QR_code"
    ],
    "properties": {
        "Question_Bank": {
            "type": "array",
            "id": "/properties/Question_Bank",
            "title": "Question Bank",
            "format": "tabs",
            "options": {
                "disable_collapse": false,
                "disable_array_add": false,
                "disable_array_delete": false,
                "disable_array_reorder": false,
                "disable_properties": false,
                "collapsed": false,
                "hidden": false
            },
            "items": {
                "type": "object",
                "id": "/properties/Question_Bank/items",
                "title": "Question ",
                "options": {
                    "disable_collapse": false,
                    "disable_edit_json": false,
                    "disable_properties": false,
                    "collapsed": false,
                    "hidden": false
                },
                "defaultProperties": [
                    "Questions"
                ],
                "properties": {
                    "Questions": {
                       // "type": "array",
                        "id": "/properties/Question_Bank/items/properties/Questions",
                        "title": "Questions",
                        "format": "tabs",
                        "oneOf": [
                            {
                              "title": "radiogroup",
                              $ref: "#/definitions/radio_Optionslist"
                            },
                            {
                              "title": "checkbox",
                              $ref: "#/definitions/check_Optionslist"
                            },
                            {
                                "title": "dropdown",
                                $ref: "#/definitions/dropdown_Optionslist"
                            },
                            {
                                "title": "text",
                                $ref: "#/definitions/text_Optionslist"
                            },
                            {
                                "title": "boolean",
                                $ref: "#/definitions/boolean_Optionslist"
                            },
                            {
                                "title": "rating",
                                $ref: "#/definitions/rating_Optionslist"
                            },
                            {
                                "title": "comment",
                                $ref: "#/definitions/comment_Optionslist"
                            }
                            ],
                        // "options": {
                        //     "disable_collapse": false,
                        //     "disable_array_add": false,
                        //     "disable_array_delete": false,
                        //     "disable_array_reorder": false,
                        //     "disable_properties": false,
                        //     "collapsed": false,
                        //     "hidden": false
                        // },
                        "propertyOrder": 10
                    }
                }
            },
            "propertyOrder": 10
        },
        "QR_code": {
            "type": "string",
            "id": "/properties/QR_code",
            "title": "QR Code",
            "default": "",
            "format": "text",
            "description": "Description for 'QR_code' Type: 'string' Path: '/properties/QR_code'",
            "options": {
                "hidden": false
            },
            "propertyOrder": 20
        }
    }
};
