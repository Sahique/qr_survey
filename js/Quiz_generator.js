
var QQ=[];
document.getElementById('inputfile') 
    .addEventListener('change', function() { 
    
    var fr=new FileReader(); 
    fr.onload=function(){ 
    QQ=document.getElementById('output').textContent=fr.result; 
    //console.log(QQ);
    myQuestions=JSON.parse(QQ); console.log(myQuestions);
    } 
    fr.readAsText(this.files[0]); 
})


///////////////////////////////////////////////

var myQuestions1 = [
    {question: "What is 10/2?",answers: {a: "3",b: "5",c: "115"},correctAnswer: "b"},
    {question: "What is 30/3?",answers: {a: '3',b: '5',c: '10'},correctAnswer: 'c'}
];
console.log("myQuestions1="+ myQuestions1);
var myQuestions=[];
var collAns=[];
var quizContainer = document.getElementById('quiz');
var resultsContainer = document.getElementById('results');
var submitButton = document.getElementById('submit');



function showQuestions(questions, quizContainer){
    // we'll need a place to store the output and the answer choices
    var output = [];
    var answers;
    var opt_type;

    // for each question...
    for(var i=0; i<questions.length; i++){
        console.log(questions[i].question);
        // first reset the list of answers
        answers = [];
        opt_type=questions[i].option_type

        // for each available answer...
        for(letter in questions[i].answers){

            // ...add an html radio button
            answers.push(
                '<label>'
                    + '<input type="'+opt_type+'" name="question'+i+'" value="'+letter+'">'
                    + letter + ': '
                    + questions[i].answers[letter]
                + '</label>'
            );
        }
        

        // add this question and its answers to the output
        output.push(
            '<div class="question">' +(i+1)+"] "+ questions[i].question + '</div>'
            + '<div class="answers">' +"   "+ answers.join('') + '</div>'
        );
    }

    // finally combine our output list into one string of html and put it on the page
    quizContainer.innerHTML = output.join('');
}


function showResults(questions, quizContainer, resultsContainer){
        
    // gather answer containers from our quiz
    var answerContainers = quizContainer.querySelectorAll('.answers');
    
    // keep track of user's answers
    var userAnswer = "";
    var numCorrect = 0;
    
    // for each question...
    for(var i=0; i<questions.length; i++){

        // find selected answer
        userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
        console.log("userans:"+userAnswer);

        /*      WORKING CODE FOR GETTING ALL THE CHECK BOXES*/

        const checkboxes = document.querySelectorAll(`input[name="${"question"+i}"]:checked`);
        //const checkboxes = (answerContainers[i].querySelectorAll('input[name=question'+i+']:checked')||{}).value;
        let values = [];
        checkboxes.forEach((checkbox) => {
        values.push(checkbox.value);
        });
       // console.log("V:"+values);
       var result=0;  // '0' means wrong answer, '1' means right answer.
       for(x=0;x<values.length;x++){console.log("V:"+values[x]);}
       for(y=0;y<values.length;y++){
            var str=JSON.stringify(myQuestions[i].correctAnswer); console.log("str: "+str);
            var output= str.indexOf(values[y]);
            if (output!=(-1)){console.log("found:"+values[y]);result=1}
            else{result=0;break;}
       }
       if (result==0) { answerContainers[i].style.color = 'red';}
       else {numCorrect++;answerContainers[i].style.color = 'lightgreen';}




        for (let [key, value] of Object.entries(myQuestions[i].answers)) {
        //console.log(`${key}: ${value}`);
        //console.log("key length:"+key.length);
        //console.log(typeof key);
            if (key==userAnswer){
               // console.log(`${key}: ${value}`);
                console.log(value);
                collAns.push(({[i]:value}));
            }

          }
        //console.log(typeof myQuestions[i].answers);
        //console.log(typeof userAnswer);
        
        // if answer is correct
        /*if(userAnswer===questions[i].correctAnswer){
            // add to the number of correct answers
            numCorrect++;
            
            // color the answers green
            answerContainers[i].style.color = 'lightgreen';
        }
        // if answer is wrong or blank
        else{
            // color the answers red
            answerContainers[i].style.color = 'red';
        }*/
    }

    // show number of correct answers out of total
    resultsContainer.innerHTML = numCorrect + ' out of ' + questions.length;
}

//showQuestions(myQuestions, quizContainer)

function result(){  
    showResults(myQuestions, quizContainer, resultsContainer);  
    console.log(collAns);  
}



